import React from "react";
import { Button, Form, Input, message, Select } from "antd";
import { postRegister } from "../../service/userService";
import { SET_USER_REGISTER } from "../../redux/constant/userContant";
import { userLocalService } from "../../service/localService";
import Lottie from "lottie-react";
import bg_animate from "../../assets/118611-cinema.json";
import { useDispatch } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import Bg_Login from "../../assets/BG_Login.jpg";
import { Option } from "antd/es/mentions";
import { UserOutlined } from "@ant-design/icons/lib/icons";
export default function Register() {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const [form] = Form.useForm();
  const onFinish = (values) => {
    postRegister(values)
      .then((res) => {
        message.success("Đăng kí thành công");
        dispatch({ type: SET_USER_REGISTER, payload: res.data.content });
        userLocalService.set(res.data.content);
        setTimeout(() => {
          navigate("/login");
        }, 1000);
      })
      .catch((er) => {
        message.error("Đăng kí thất bại");
      });
  };
  const onFinishFailed = (errorInfo) => {};
  const prefixSelector = (
    <Form.Item name="prefix" noStyle>
      <Select
        style={{
          width: 70,
        }}
      >
        <Option value="84">
          {" "}
          <span className="font-medium">+84</span>
        </Option>
      </Select>
    </Form.Item>
  );
  const formItemLayout = {
    labelCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 8,
      },
    },
    wrapperCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 16,
      },
    },
  };
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  };

  return (
    <div className="h-screen w-screen  ">
      <img
        className="absolute h-full bg-opacity-75  -z-10"
        src={Bg_Login}
        alt=""
      />
      <div className=" fixed opacity-90 w-screen  ">
        <div className="bg-[#A6BB8D] px-10 py-5 rounded-3xl flex justify-center   items-center ">
          <div className="w-1/2 hidden">
            <Lottie animationData={bg_animate} />
          </div>
          <div className="">
            <UserOutlined
              style={{
                fontSize: "50px",
                marginLeft: "120px",
                textAlign: "center",
                color: "#3C6255",
              }}
            />
            <Form
              className=" text-left"
              {...formItemLayout}
              form={form}
              name="register"
              onFinish={onFinish}
              scrollToFirstError
            >
              <Form.Item
                className="font-medium"
                label="Tài Khoản"
                name="taiKhoan"
                rules={[
                  {
                    required: true,
                    message: (
                      <div className="text-blue-500 font-bold">
                        Xin Vui Lòng Nhập Tài Khoản
                      </div>
                    ),
                  },
                ]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                className="font-medium"
                name="matKhau"
                label="Password"
                rules={[
                  {
                    required: true,
                    message: (
                      <div className="text-blue-500 font-bold">
                        Xin Vui Lòng Nhập Mật Khẩu
                      </div>
                    ),
                  },
                ]}
                hasFeedback
              >
                <Input.Password />
              </Form.Item>
              <Form.Item
                className="font-medium"
                name="email"
                label="E-mail"
                rules={[
                  {
                    type: "email",
                    message: (
                      <div className="text-blue-500 font-bold">
                        Đây Không Phải là Email VD:(@gmail.com,
                        @yahoo.com,.....) ,
                      </div>
                    ),
                  },
                  {
                    required: true,
                    message: (
                      <div className="text-blue-500 font-bold">
                        Xin Vui Lòng Nhập E-mail ,
                      </div>
                    ),
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                className="font-medium"
                name="hoTen"
                label="Tên Tài Khoản"
                tooltip="Tên Tài Khoản Được Hiển Thị"
                rules={[
                  {
                    required: true,
                    message: (
                      <div className="text-blue-500 font-bold">
                        Xin Vui Lòng Nhập Tên Tài Khoản ,
                      </div>
                    ),
                    whitespace: true,
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                className="font-medium"
                name="soDt"
                label="Số Điện Thoại"
                rules={[
                  {
                    required: true,
                    message: (
                      <div className="text-blue-500 font-bold">
                        Xin Vui Lòng Nhập Số Điện Thoại ,
                      </div>
                    ),
                  },
                ]}
              >
                <Input
                  addonBefore={prefixSelector}
                  style={{
                    width: "100%",
                  }}
                />
              </Form.Item>

              <Form.Item {...tailFormItemLayout}>
                <Button
                  className="bg-[#22A39F] font-medium ml-10 text-white mr-2  "
                  htmlType="submit"
                >
                  Đăng Ký
                </Button>
                <Button className="bg-[#61876E] font-medium text-white  ">
                  <NavLink to={"/login"}>Đăng Nhập</NavLink>
                </Button>
              </Form.Item>
              <NavLink className=" " to={"/"}>
                <button className="text-center font-black  bg-[#F3EFE0]  rounded-3xl w-72 px-5  py-1 hover:text-[#22A39F] duration-1000">
                  Về Lại Trang Chủ Nhấn Vào Đây
                </button>
              </NavLink>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
}
