import React from "react";
import { Desktop, Mobile, Tablet } from "../../HOC/Reponsive";
import RegisterDesktop from "./RegisterDesktop";
import RegisterTablet from "./RegisterTablet";
import RegisterMobile from "./RegisterMobile";

export default function Register() {
  return (
    <div>
      <Desktop>
        <RegisterDesktop />
      </Desktop>
      <Tablet>
        <RegisterTablet />
      </Tablet>
      <Mobile>
        <RegisterMobile />
      </Mobile>
    </div>
  );
}
