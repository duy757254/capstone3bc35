import React from "react";
import { Desktop, Tablet, Mobile } from "../../../HOC/Reponsive";
import DetailTabDesktop from "./DetailTabDesktop";
import DetailTabTablet from "./DetailTabTablet";
import DetailTabMobile from "./DetailTabMobile";
export default function DetailTab() {
  return (
    <div>
      <Desktop>
        <DetailTabDesktop />
      </Desktop>
      <Tablet>
        <DetailTabTablet />
      </Tablet>
      <Mobile>
        <DetailTabMobile />
      </Mobile>
    </div>
  );
}
