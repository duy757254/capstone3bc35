import React, { useEffect, useState } from "react";
import { NavLink, useParams } from "react-router-dom";
// import Header from "../../Component/Header/Header";
import { getDetailMovie } from "../../service/getDetailMovie";
import ReactPlayer from "react-player";
import moment from "moment";
import { HomeOutlined, SendOutlined } from "@ant-design/icons";
import { Breadcrumb, Rate, Button, Modal } from "antd";
import "moment/locale/vi";
import "./DetailPage.css";
import DetailTab from "./DetailTab/DetailTab";

export default function DetailPage() {
  let param = useParams();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const [detailMovie, setdetailMovie] = useState([]);

  useEffect(() => {
    getDetailMovie(param.idPhim)
      .then((res) => {
        setdetailMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const renderDetailMovie = () => {
    return (
      <div className="mt-10 mx-24 ">
        <div className="p-10 bg-[#3C6255] flex items-center  hover:drop-shadow-2xl duration-1000  rounded-2xl">
          <div className="w-2/6">
            <img
              className="  hover:rotate-12 duration-700 rounded-2xl h-72  "
              src={detailMovie.hinhAnh}
              alt=""
            />
          </div>
          <div className=" space-y-5 text-[20px] w-3/4   ">
            <h2>
              <span className="font-medium">Tên Phim:</span>{" "}
              {detailMovie.tenPhim}
            </h2>
            <h2>
              <span className="font-medium">Ngày Khởi Chiếu: </span>
              {moment(detailMovie.ngayKhoiChieu).format("LLL")}
            </h2>
            <h2>
              <span className="font-medium">Tổng Quan:</span> {detailMovie.moTa}
            </h2>

            <div className=" flex  hover:ml-7 duration-1000 ">
              <div className="text-[30px] text-pink-500 ">
                <SendOutlined />{" "}
              </div>
              <button
                className="px-4 ml-4 py-1 font-black text-xl text-black rounded-2xl bg-[#EAE7B1] "
                onClick={showModal}
              >
                Xem Trailer
              </button>
              <Modal
                width="710px"
                height="400px"
                title="Trailer Phim"
                open={isModalOpen}
                onCancel={handleCancel}
              >
                <ReactPlayer
                  width="650px"
                  height="360px"
                  // onProgress={handleProgress}
                  url={detailMovie.trailer}
                />
              </Modal>
            </div>
          </div>
          <div className=" text-right w-2/6  ">
            <Rate disabled defaultValue={5} />
          </div>
        </div>
      </div>
    );
  };

  return (
    <div className="pb-5 bg-[#61876E]">
      <div className="font-black w-3/4 mx-auto ">
        <Breadcrumb className="text-xl">
          <Breadcrumb.Item>
            <NavLink to={"/"}>
              <div className="leading-3">
                <HomeOutlined
                  style={{ fontSize: "20px", fontWeight: "bold" }}
                />
              </div>
            </NavLink>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <span>{detailMovie.tenPhim}</span>
          </Breadcrumb.Item>
        </Breadcrumb>
      </div>

      <h3 className="mx-24 my-10 text-3xl font-semibold border-b-2 z-10 hover:mx-10 duration-700 px-5 py-2  ">
        Nội Dung Phim
      </h3>

      {renderDetailMovie()}
      <h3 className="mx-24 my-10 text-3xl font-semibold border-b-2 z-10 hover:mx-10 duration-700 px-5 py-2">
        Lịch Chiếu Phim
      </h3>
      <DetailTab param={param} />
    </div>
  );
}
