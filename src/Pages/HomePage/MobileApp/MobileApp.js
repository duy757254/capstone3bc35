import React from "react";
import { Desktop, Mobile, Tablet } from "../../../HOC/Reponsive";
import MobileAppDesktop from "./MobileAppDesktop";
import MobileAppTablet from "./MobileAppTablet";
import MobileAppMobile from "./MobileAppMobile";

export default function MobileApp() {
  return (
    <div>
      <Desktop>
        <MobileAppDesktop />
      </Desktop>
      <Tablet>
        <MobileAppTablet />
      </Tablet>
      <Mobile>
        <MobileAppMobile />
      </Mobile>
    </div>
  );
}
