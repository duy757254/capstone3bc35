import React from "react";
import BG_App from "../../../assets/BG_App.jpg";
import "animate.css";
import "./MovileApp.css";

export default function MobileApp() {
  return (
    <div className="text-center px-20 font-black text-white   ">
      <div className="space-y-6 	 ">
        <div className="py-10 bg-[#3C6255] rounded-3xl">
          <h1 className="animate__fadeInRight animate__animated animate__delay-2s text-[50px]">
            {" "}
            Nhanh Chóng, Dễ Dàng
          </h1>
          <h2 className="animate__fadeInTopLeft animate__delay-3s animate__animated text-[45px]">
            Cả Thế Giới Trong Tay Bạn
          </h2>
          <h3 className="animate__fadeInRight animate__delay-4s animate__animated text-[40px]">
            Chỉ Vọn Vẹn 1 Cú CLick
          </h3>
          <h1 className="animate__fadeInTopLeft animate__delay-5s animate__animated text-[35px]">
            Tải App Ngay
          </h1>
          <button
            hre
            className="animate__fadeInRight  animate__animated mt-11 px-5 py-2 bg-red-500 rounded-2xl"
          >
            <a href="https://play.google.com/store/apps/details?id=com.netflix.mediaclient&hl=en&gl=US&pli=1">
              For Android
            </a>
          </button>
          <button
            hre
            className="animate__fadeInRight  animate__animated mt-11 ml-5 px-5 py-2 bg-white text-black rounded-2xl"
          >
            <a href="https://apps.apple.com/us/app/netflix/id363590051">
              For IOS
            </a>
          </button>
        </div>
      </div>
    </div>
  );
}
