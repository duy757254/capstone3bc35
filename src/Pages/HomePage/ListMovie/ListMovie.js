import React from "react";
import { Desktop, Mobile, Tablet } from "../../../HOC/Reponsive";
import ListMovieDesktop from "./ListMovieDesktop";
import ListMovieTablet from "./ListMovieTablet";
import ListMovieMobile from "./ListMovieMobile";
export default function ListMovie() {
  return (
    <div>
      <Desktop>
        <ListMovieDesktop />
      </Desktop>
      <Tablet>
        <ListMovieTablet />
      </Tablet>
      <Mobile>
        <ListMovieMobile />
      </Mobile>
    </div>
  );
}
