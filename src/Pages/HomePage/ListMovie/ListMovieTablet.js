import React, { useEffect, useState } from "react";
import { Card } from "antd";
import { getMovieService } from "../../../service/movieService";
import { NavLink } from "react-router-dom";
const { Meta } = Card;
export default function ListMovie() {
  const [movieArr, setMovieArr] = useState([]);

  useEffect(() => {
    getMovieService()
      .then((res) => {
        setMovieArr(res.data.content);
      })
      .catch((err) => {});
  }, []);

  const renderMovieList = () => {
    return movieArr.slice(0, 15).map((movie) => {
      return (
        <Card
          hoverable
          className="text-center m-auto "
          style={{
            width: 240,
          }}
          cover={<img className="h-80" alt="example" src={movie.hinhAnh} />}
        >
          <Meta
            title={
              <h2 className="text-[#00ADB5] font-medium">{movie.tenPhim}</h2>
            }
            description={
              <p className=" h-20">
                {movie.moTa.length < 60
                  ? movie.moTa
                  : movie.moTa.slice(0, 60) + "..."}
              </p>
            }
          />
          <NavLink
            className="px-6 mt-10 py-3 rounded-xl text-[#EEEEEE] bg-[#222831]"
            to={`/detail/${movie.maPhim}`}
          >
            Xem Chi Tiết
          </NavLink>
        </Card>
      );
    });
  };

  return (
    <div className=" mb-10 grid grid-cols-3 items-center gap-2 px-32x ">
      {renderMovieList()}
    </div>
  );
}
