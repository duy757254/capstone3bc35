import React from "react";
import { Carousel } from "antd";
import SlideMovie from "./SlideMovie.js/SlideMovie";
import ListMovie from "./ListMovie/ListMovie";
import MovieTab from "./MovieTab/MovieTab";
import "animate.css";
import MobileApp from "./MobileApp/MobileApp";

export default function HomePage() {
  return (
    <div className="bg-[#61876E] pb-5">
      <div className="mb-10">
        <SlideMovie />
      </div>

      <div className="mb-20">
        <ListMovie />
      </div>
      <div className="py-10 ">
        <MovieTab />
      </div>
      <div>
        <MobileApp />
      </div>
    </div>
  );
}
