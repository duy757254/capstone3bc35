import React from "react";
import { Desktop, Mobile, Tablet } from "../../../../HOC/Reponsive";
import MovieItemTabDesktop from "./MovieItemTabDesktop";
import MovieItemTabTablet from "./MovieItemTabTablet";
import MovieItemTabMobile from "./MovieItemTabMobile";
export default function MovieItemTab({ movie }) {
  return (
    <div>
      <Desktop>
        <MovieItemTabDesktop movie={movie} />
      </Desktop>
      <Tablet>
        <MovieItemTabTablet movie={movie} />
      </Tablet>
      <Mobile>
        <MovieItemTabMobile movie={movie} />
      </Mobile>
    </div>
  );
}
