import moment from "moment";
import React from "react";

export default function MovieItemTab({ movie }) {
  return (
    <div className=" mt-10 items-center ">
      <img
        className="h-40 w-24 border rounded-lg "
        src={movie.hinhAnh}
        alt=""
      />
      <div>
        <div>
          <h4 className="font-black">{movie.tenPhim}</h4>
        </div>
        <div className=" grid text-center grid-cols-1 gap-2">
          {movie.lstLichChieuTheoPhim?.slice(0, 2).map((lichChieu) => {
            return (
              <p className="bg-[#EAE7B1] text-black mt-5  px-1 py-2 rounded-xl font-medium">
                {moment(lichChieu.ngayChieuGioChieu).format("DD/MM hh:mm A")}
              </p>
            );
          })}
        </div>
      </div>
    </div>
  );
}
