import React, { Children, useEffect, useState } from "react";
import { Tabs } from "antd";
import { getShowTimes } from "../../../service/movieService";
import MovieItemTab from "./MovieItemTab/MovieItemTab";

export default function MovieTab() {
  const [ArrFilm, setArrFilm] = useState([]);
  useEffect(() => {
    getShowTimes()
      .then((res) => {
        setArrFilm(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const onChange = (key) => {
    console.log(key);
  };
  const renderHeThongRap = () => {
    return ArrFilm.map((hethongrap) => {
      return {
        label: <img className="w-10 h-10" src={hethongrap.logo} alt="" />,
        key: hethongrap.maHeThongRap,
        children: (
          <Tabs
            style={{
              height: "500px",
            }}
            tabPosition="left"
            defaultActiveKey="1"
            onChange={onChange}
            items={renderCumRapTheoHeThongRap(hethongrap)}
          />
        ),
      };
    });
  };

  const renderCumRapTheoHeThongRap = (hethongrap) => {
    return hethongrap.lstCumRap?.map((cumRap) => {
      return {
        label: (
          <div className="active:text-white w-10 text-left px-3 hover:text-[#EAE7B1] hover:w-72 duration-1000">
            <h2 className="font-bold">{cumRap.tenCumRap}</h2>
            <p className="truncate">{cumRap.diaChi}</p>
          </div>
        ),
        key: cumRap.maCumRap,
        children: (
          <div
            style={{
              height: "500px",
              overflow: "scroll",
            }}
          >
            {renderLichChieuPhimTheoCumRap(cumRap)}
          </div>
        ),
      };
    });
  };
  const renderLichChieuPhimTheoCumRap = (cumRap) => {
    return cumRap.danhSachPhim.map((movie) => {
      console.log("movie: ", movie);
      return <MovieItemTab movie={movie} />;
    });
  };

  return (
    <div className=" py-10 hover:drop-shadow-2xl duration-1000  rounded-2xl bg-[#3C6255] mx-10">
      <Tabs
        style={{ height: "500px" }}
        tabPosition="left"
        defaultActiveKey="1"
        items={renderHeThongRap()}
        onChange={onChange}
      />
    </div>
  );
}
