import { Table } from "antd";
import React from "react";
import { Desktop, Mobile, Tablet } from "../../../HOC/Reponsive";
import MovieTabDesktop from "./MovieTabDesktop";
import MovieTabMobile from "./MovieTabMobile";
import MovieTabTablet from "./MovieTabTablet";
export default function MovieTab() {
  return (
    <div>
      <Desktop>
        <MovieTabDesktop />
      </Desktop>
      <Tablet>
        <MovieTabTablet />
      </Tablet>
      <Mobile>
        <MovieTabMobile />
      </Mobile>
    </div>
  );
}
