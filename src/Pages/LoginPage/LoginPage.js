import React from "react";
import { Desktop, Mobile, Tablet } from "../../HOC/Reponsive";
import LoginPageDesktop from "./LoginPageDesktop";
import LoginPageMobile from "./LoginPageMobile";
import LoginPageTablet from "./LoginPageTablet";

export default function LoginPage() {
  return (
    <div>
      <Desktop>
        <LoginPageDesktop />
      </Desktop>
      <Tablet>
        <LoginPageTablet />
      </Tablet>
      <Mobile>
        <LoginPageMobile />
      </Mobile>
    </div>
  );
}
