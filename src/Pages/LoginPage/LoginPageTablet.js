import React from "react";
import { Button, Form, Input, message } from "antd";
import { postLogin } from "../../service/userService";
import { NavLink, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { SET_USER_LOGIN } from "../../redux/constant/userContant";
import { userLocalService } from "../../service/localService";
import Lottie from "lottie-react";
import bg_animate from "../../assets/118611-cinema.json";
import Bg_Login from "../../assets/BG_Login.jpg";
import { QqOutlined } from "@ant-design/icons";
import { UserOutlined } from "@ant-design/icons/lib/icons";
export default function LoginPage() {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const onFinish = (values) => {
    postLogin(values)
      .then((res) => {
        message.success("Đăng nhập thành công");
        dispatch({ type: SET_USER_LOGIN, payload: res.data.content });
        userLocalService.set(res.data.content);
        setTimeout(() => {
          navigate("/");
        }, 1000);
      })
      .catch((err) => {
        message.error("Đăng nhập thất bại");
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    //
    <div className="   ">
      <img
        className="absolute w-screen h-full  bg-opacity-75  -z-10"
        src={Bg_Login}
        alt=""
      />
      <div className="h-screen fixed opacity-90 w-screen px-24 py-32">
        <div className="bg-[#A6BB8D]  py-10 px-10 rounded-3xl flex justify-center items-center ">
          <div className="w-1/2">
            <Lottie animationData={bg_animate} />
          </div>
          <div className="w-1/2 ">
            <UserOutlined
              style={{
                fontSize: "100px",
                marginBottom: "20px",
                textAlign: "center",
                color: "#CFB997",
              }}
            />

            <Form
              name="basic"
              labelCol={{
                span: 8,
              }}
              wrapperCol={{
                span: 15,
              }}
              initialValues={{
                remember: true,
              }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                className="font-medium"
                label="Tài Khoản"
                name="taiKhoan"
                rules={[
                  {
                    required: true,
                    message: (
                      <div className="text-[#3C6255] font-bold">
                        Xin Vui Lòng Nhập Tài Khoản
                      </div>
                    ),
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                className="font-medium"
                label="Mật Khẩu"
                name="matKhau"
                rules={[
                  {
                    required: true,
                    message: "Xin Vui Lòng Nhập Mật Khẩu",
                  },
                ]}
              >
                <Input.Password />
              </Form.Item>

              <Form.Item
                className="mr-14"
                wrapperCol={{
                  offset: 8,
                  span: 8,
                }}
              >
                <div className="flex  gap-2">
                  <Button
                    className="bg-[#61876E] font-medium text-white  "
                    htmlType="submit"
                    // htmlType="submit"
                  >
                    Đăng Nhập
                  </Button>
                  <Button className="bg-[#22A39F] font-medium text-white  ">
                    <NavLink to={"/register"}>Đăng Ký</NavLink>
                  </Button>
                </div>
              </Form.Item>
              <NavLink to={"/"}>
                <button className="text-center font-black bg-[#F3EFE0] ml-28 rounded-3xl px-5 py-2 hover:text-[#22A39F] duration-1000">
                  Về Lại Trang Chủ Nhấn Vào Đây
                </button>
              </NavLink>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
}
