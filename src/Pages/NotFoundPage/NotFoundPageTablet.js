import React from "react";
import { NavLink } from "react-router-dom";

export default function NotFoundPage() {
  return (
    <div className="py-32 px-56 text-center bg-gradient-to-r from-indigo-500 via-purple-500 to-pink-500 h-screen w-screen  ">
      <div className="bg-white px-10 mt-10 rounded-2xl  opacity-60">
        <h1 className=" animate-bounce font-black font-size text-[180px] text-center text-transparent bg-clip-text bg-gradient-to-tr from-pink-500 via-purple-300 to-indigo-400  ">
          404
        </h1>
        <br />
        <p className="mb-10  text-[25px] font-bold  text-transparent bg-gradient-to-r from-green-200 via-green-400 to-purple-700 bg-clip-text ">
          Trang không được tìm thấy hoặc không tồn tại!! <br />
          Nhấn vào nút bên dưới để trở lại
        </p>
        <NavLink to={"/"}>
          <button className="mb-10 cursor-pointer bg-[#393E46] px-5 py-3  rounded-2xl text-[#00ADB5] font-extrabold hover:text-pink-500 duration-[1000ms]">
            Quay Lại Trang Chủ
          </button>
        </NavLink>
      </div>

      {/* <img className="h-screen w-screen opacity-25" src={BG_404} alt="" /> */}

      {/* <NavLink className=" px-14 py-10  bg-[#00ADB5] text-center" to={"/"}>
        <div>
          <h1 className="  text-[50px]">404 NotFoundPage</h1>
          <p>Nhấn vào Để Quay Trở Lại Trang Chủ</p>
        </div>
      </NavLink> */}
    </div>
  );
}

// export default function NotFoundPage() {
//   return (
//     <div className="h-screen w-screen flex justify-center items-center text-center text-black animate-pulse text-5xl">
//       404 Page
//     </div>
//   );
// }
