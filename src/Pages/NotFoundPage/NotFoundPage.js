import React from "react";
import { Desktop, Mobile, Tablet } from "../../HOC/Reponsive";
import NotFoundPageDesktop from "./NotFoundPageDesktop";
import NotFoundPageTablet from "./NotFoundPageTablet";
import NotFoundPageMobile from "./NotFoundPageMobile";

export default function NotFoundPage() {
  return (
    <div>
      <Desktop>
        <NotFoundPageDesktop />
      </Desktop>
      <Tablet>
        <NotFoundPageTablet />
      </Tablet>
      <Mobile>
        <NotFoundPageMobile />
      </Mobile>
    </div>
  );
}
