import { HTTPS } from "./configURL";

export const getDetailMovie = (data) => {
  return HTTPS.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${data}`);
};
