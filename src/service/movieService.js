import { HTTPS } from "./configURL";
// api lấy list phim
export const getMovieService = () => {
  return HTTPS.get("api/QuanLyPhim/LayDanhSachPhim?maNhom=GP07");
};
// api lấy thông tin từng phim
export const movieTheater = (data) => {
  return HTTPS.get(`/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${data}`);
};
// api lấy danh sách banner
export const slideMovie = () => {
  return HTTPS.get(`/api/QuanLyPhim/LayDanhSachBanner`);
};
export const getShowTimes = () => {
  return HTTPS.get(`/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP07`);
};
