import React from "react";
import { useSelector } from "react-redux";

import { userLocalService } from "../../../service/localService";

export default function UserNav() {
  let user = useSelector((state) => {
    return state.userReducer.user;
  });
  const handleLogout = () => {
    userLocalService.remove();
    window.location.reload();
  };
  const renderContent = () => {
    if (user) {
      // đã đăng nhập
      return (
        <>
          <span className="text-white font-bold">{user?.hoTen}</span>
          <button
            onClick={handleLogout}
            className="border bg-teal-700 text-white px-5 py-2 rounded"
          >
            Đăng xuất
          </button>
        </>
      );
    } else {
      return (
        <>
          <button
            onClick={() => {
              window.location.href = "/login";
            }}
            className="border bg-teal-700 active:bg-violet-700 hover:bg-[#88A47C] duration-1000 text-white px-5 py-2 rounded hover:"
          >
            Đăng nhập
          </button>

          <button
            onClick={() => {
              window.location.href = "/register";
            }}
            className="border bg-teal-700 text-white px-5 py-2 rounded hover:bg-[#88A47C] duration-1000 active:bg-violet-700"
          >
            Đăng ký
          </button>
        </>
      );
    }
  };
  return <div className="space-x-3">{renderContent()}</div>;
}
