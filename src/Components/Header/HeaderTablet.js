import React from "react";
import { NavLink } from "react-router-dom";
import UserNav from "./UserNav/UserNav";
import logo_movie from "../../assets/Cinema HD Logo.png";

export default function Header() {
  return (
    <div className="px-10 fixed z-10 opacity-90 w-screen top-0  py-3 flex justify-between items-center shadow-lg bg-[#3C6255]">
      <NavLink to={"/"}>
        <img
          className="w-[70px]  mx-auto hover:opacity-60 duration-1000 "
          src={logo_movie}
          alt=""
        />
      </NavLink>
      <NavLink className="nav">
        <ul>
          <li className="active">
            <a href="/" className="font-bold">
              Lịch Chiếu
            </a>
          </li>
          <li>
            <a href="/" className="font-bold">
              Cụm Rạp
            </a>
          </li>
          <li>
            <a href="/" className="font-bold">
              Tin Tức
            </a>
          </li>
          <li>
            <a href="/" className="font-bold">
              Ứng dụng
            </a>
          </li>
        </ul>
      </NavLink>

      <UserNav />
    </div>
  );
}
