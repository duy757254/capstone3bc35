import React from "react";
import { Desktop, Mobile, Tablet } from "../../HOC/Reponsive";
import FooterPageDesktop from "./FooterPageDesktop";
import FooterPageMobile from "./FooterPageMobile";
import FooterPageTablet from "./FooterPageTablet";

export default function FooterPage() {
  return (
    <div>
      <Desktop>
        <FooterPageDesktop />
      </Desktop>
      <Tablet>
        <FooterPageTablet />
      </Tablet>
      <Mobile>
        <FooterPageMobile />
      </Mobile>
    </div>
  );
}
